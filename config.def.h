/* See LICENSE file for copyright and license details. */

// INCLUDES
#include "fibonacci.c"
#include <X11/XF86keysym.h>

/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "monospace:size=10" };
static const char dmenufont[]       = "monospace:size=10";
static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#005577";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class                  instance          title       tags mask     isfloating      monitor */
	{ "Gimp",                 NULL,             NULL,       1 << 0,       0,              -1 },
	{ "jetbrains-clion",      NULL,             NULL,       1 << 0,       0,              -1 },
	{ "VirtualBox Manager",   NULL,             NULL,       1 << 1,       1,              -1 },
	{ "Pcmanfm",              NULL,             NULL,       1 << 2,       0,              -1 },
	{ "Vivaldi-stable",       NULL,             NULL,       1 << 9,       0,              -1 },
};

/* layout(s) */
static const float mfact     = 0.70;     /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;        /* number of clients in master area */
static const int resizehints = 0;        /* 1 means respect size hints in tiled resizals */
static const int attachdirection = 2;    /* 0 default, 1 above, 2 aside, 3 below, 4 bottom, 5 top */
static const int lockfullscreen = 1;     /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },                   /* 0 first entry is default */
	{ "><>",      NULL },                   /* 1 no layout function means floating behavior */
	{ "[M]",      monocle },                /* 2 */
	{ "[@]",      spiral },                 /* 3 */
	{ "[\\]",     dwindle },                /* 4 */
	{ "|M|",      centeredmaster },         /* 5 */
	{ ">M>",      centeredfloatingmaster },	/* 6 */
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *termcmd[]  = { "st", NULL };
static const char *screenshotcmd[] = { "flameshot", "gui", NULL };
/* brightness */
static const char *brupcmd[] = { "xbacklight", "-inc", "1", NULL };
static const char *brdowncmd[] = { "xbacklight", "-dec", "1", NULL };
	// jump bigger steps with shift
static const char *brupjpcmd[] = { "xbacklight", "-inc", "10", NULL };
static const char *brdownjpcmd[] = { "xbacklight", "-dec", "10", NULL };
static const char *brnullcmd[] = { "xbacklight", "-set", "0", NULL };
/* toggle TouchPad F11 */
static const char *toggletouchcmd[] = { "toggleTouch.sh" };
/* music */
static const char *playerctltogglecmd[] = { "playerctl", "-p", "spotify", "play-pause", NULL };
static const char *playerctlstopcmd[] = { "playerctl", "-p", "spotify", "stop", NULL };
static const char *playerctlnextcmd[] = { "playerctl", "-p", "spotify", "next", NULL };
static const char *playerctlprevcmd[] = { "playerctl", "-p", "spotify", "previous", NULL };

static Key keys[] = {
	/* modifier                     key         function        argument */
	{ MODKEY,                       XK_p,       spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_Return,  spawn,          {.v = termcmd } },
	{ 0,                            XK_Print,   spawn,          {.v = screenshotcmd } },
	{ MODKEY,                       XK_b,       togglebar,      {0} },
	{ MODKEY,                       XK_j,       focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,       focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,       incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,       incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,       setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,       setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_Return,  zoom,           {0} },
	{ MODKEY,                       XK_Tab,     view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,       killclient,     {0} },
	{ MODKEY,                       XK_t,       setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,       setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,       setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_r,       setlayout,      {.v = &layouts[4]} },
	{ MODKEY|ShiftMask,             XK_r,       setlayout,      {.v = &layouts[5]} },
	{ MODKEY,                       XK_u,       setlayout,      {.v = &layouts[3]} },
	{ MODKEY,                       XK_o,       setlayout,      {.v = &layouts[6]} },
	{ MODKEY,                       XK_space,   setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,   togglefloating, {0} },
	{ MODKEY,                       XK_minus,   view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_minus,   tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,   focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period,  focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,   tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period,  tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_1,                       0)
	TAGKEYS(                        XK_2,                       1)
	TAGKEYS(                        XK_3,                       2)
	TAGKEYS(                        XK_4,                       3)
	TAGKEYS(                        XK_5,                       4)
	TAGKEYS(                        XK_6,                       5)
	TAGKEYS(                        XK_7,                       6)
	TAGKEYS(                        XK_8,                       7)
	TAGKEYS(                        XK_9,                       8)
	TAGKEYS(                        XK_0,                       9)
	{ MODKEY|ShiftMask,             XK_q,       quit,           {0} },
	/* audio */
	{ 0,              XF86XK_AudioMute,         spawn,          SHCMD("amixer -q set Master toggle; kill -44 $(pidof dwmblocks)") },
	{ 0,              XF86XK_AudioRaiseVolume,  spawn,          SHCMD("amixer -q set Master 1%+ unmute; kill -44 $(pidof dwmblocks)") },
	{ 0,              XF86XK_AudioLowerVolume,  spawn,          SHCMD("amixer -q set Master 1%- unmute; kill -44 $(pidof dwmblocks)") },
	{ ShiftMask,      XF86XK_AudioRaiseVolume,  spawn,          SHCMD("amixer -q set Master 5%+ unmute; kill -44 $(pidof dwmblocks)") },
	{ ShiftMask,      XF86XK_AudioLowerVolume,  spawn,          SHCMD("amixer -q set Master 5%- unmute; kill -44 $(pidof dwmblocks)") },
	{ 0,              XF86XK_AudioMicMute,      spawn,          SHCMD("amixer set Capture toggle;") },
	/* brightness */
	{ 0,              XF86XK_MonBrightnessUp,   spawn,          {.v = brupcmd} },
	{ 0,              XF86XK_MonBrightnessDown, spawn,          {.v = brdowncmd} },
	{ ShiftMask,      XF86XK_MonBrightnessUp,   spawn,          {.v = brupjpcmd} },
	{ ShiftMask,      XF86XK_MonBrightnessDown, spawn,          {.v = brdownjpcmd} },
	{ 0,              XF86XK_Display,           spawn,          {.v = brnullcmd} },
	/* toggle TouchPad */
	{ 0,              XF86XK_Launch2,           spawn,          {.v = toggletouchcmd} },
	/* music */
	{ 0,              XF86XK_AudioPlay,         spawn,          {.v = playerctltogglecmd} },
	{ 0,              XF86XK_AudioStop,         spawn,          {.v = playerctlstopcmd} },
	{ 0,              XF86XK_AudioNext,         spawn,          {.v = playerctlnextcmd} },
	{ 0,              XF86XK_AudioPrev,         spawn,          {.v = playerctlprevcmd} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

